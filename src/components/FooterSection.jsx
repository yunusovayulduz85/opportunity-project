import React from 'react'
import LogoSvg from '../icons/LogoSvg'

const FooterSection = () => {
    return (
        <section id='blog' className='flex justify-center pt-8'>
            <div className='container'>
                <div className='flex flex-col justify-center items-center'>
                    <LogoSvg props={'footer'} />
                    <h1 className='text-white text-3xl lg:text-5xl font-extrabold py-3 tracking-wide'>Get Started Now</h1>
                    <p className='text-white tracking-widest mb-3 text-xl text-center'>Setup is easy and takes under 5 minutes.</p>
                    <button className='btn btn-primary lg:px-12 lg:py-3'>Get Started Now</button>
                </div>
                <div className='flex items-center gap-2 justify-center pt-4'>
                    <div className='w-6 h-6 rounded-full header-point flex justify-center items-center'>
                        <div className='w-3 h-3 rounded-full bg-headerPoint'></div>
                    </div>
                    <p className='text-smallText tracking-wider text-lg'>
                        <span className='font-bold text-white text-sm md:text-base'>1000+</span>
                        creators have already started
                    </p>
                </div>
                <footer className='responsiveFooter flex justify-between px-20 py-8 mt-16 items-center'>
                    <div>
                        <div className='flex items-center gap-3'>
                            <LogoSvg props={'navbar'} />
                            <p className='text-white font-bold text-2xl cursor-pointer'>Marico</p>
                        </div>
                        <p className='text-smallText'>info@marico.co</p>
                    </div>
                    <div>
                        <ul className='flex items-center text-smallText gap-6'>
                            <li className='cursor-pointer hover:text-blue-500 duration-200'>About</li>
                            <li className='cursor-pointer hover:text-blue-500 duration-200'>Pricing</li>
                            <li className='cursor-pointer hover:text-blue-500 duration-200'>Blog</li>
                            <li className='cursor-pointer hover:text-blue-500 duration-200'>Sign in</li>
                        </ul>
                    </div>
                </footer>
            </div>
        </section>
    )
}

export default FooterSection