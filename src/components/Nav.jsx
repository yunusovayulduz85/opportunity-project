import React, { useEffect, useState } from 'react'
import LogoSvg from '../icons/LogoSvg'
import MenuSvg from '../icons/MenuSvg'
const menuLinks = [
    {
        to: "usecases",
        linkName: "Use Cases"
    },
    {
        to: "about",
        linkName: "About"
    },
    {
        to: "pricing",
        linkName: "Pricing"
    },
    {
        to: "blog",
        linkName: "blog"
    },
]
let counter = 0;
const Nav = () => {
    const [showMenu, setShowMenu] = useState(false)
    const [scrolled, setScrolled] = useState(false)
    const scrollToSection = (id) => {
        document.getElementById(id).scrollIntoView({ behavior: 'smooth' });
      };
    useEffect(() => {
        function handleScroll() {
            if (window.scrollY > 0) setScrolled(true)
            else setScrolled(false)
        }
        window.addEventListener('scroll', handleScroll)
        return () => {
            window.removeEventListener('scroll', handleScroll)
        }
    }, [])
    const handleMenuBar = () => {
        counter++;
        if (counter % 2 !== 0) setShowMenu(true)
        else setShowMenu(false)
    }
    return (
        <nav className={`${scrolled && 'scrolled'} fixed top-0 left-0 w-full h-navHeight bg-transparent z-50 flex justify-center`}>
            <div className='container'>
                <div className='navbar h-full w-full flex items-center justify-between'>
                    <div onClick={() => scrollToSection('header')} className='flex items-center gap-3'>
                        <LogoSvg props={'navbar'} />
                        <h3 className='text-white font-bold text-2xl cursor-pointer'>Marico</h3>
                    </div>
                    <div>
                        <ul className={`${showMenu && 'fixed -mt- 9 -ml- 16 px-6 rounded-md menuBg pt-8 pb-8 flex-col gap-5 '} hidden md:flex md:flex-row md:pt-0  md:bg-transparent md:gap-5 md:mt-0 md:pb-0 md:ml-0 md:relative`}>
                            {
                                menuLinks?.map((item, index) => {
                                    return <li className='text-white text-center md:text-smallText cursor-pointer flex items-center gap-1 hover:text-blue-500 duration-200'><button onClick={() => scrollToSection(item.to)}> {item.linkName}</button></li>
                                })
                            }
                        </ul>
                        <ul className={`${showMenu === true ? 'border dropdownMenu fixed mt-4 ml-2 sm:ml-10 px-6 rounded-md menuBg pt-8 pb-8 flex flex-col gap-5 ' : 'hidden'} menuBar2`}>
                            {
                                menuLinks?.map((item, index) => {
                                    return <li className='text-white text-center md:text-smallText cursor-pointer flex items-center gap-1 hover:text-blue-500 duration-200'><button onClick={() => scrollToSection(item.to)}> {item.linkName}</button></li>
                                })
                            }
                        </ul>

                    </div>
                    <button onClick={handleMenuBar} className='flex md:hidden text-white'>
                        <MenuSvg />
                    </button>

                    {/* <div className='hidden md:flex items-center gap-3'>
                        <button onClick={() => toggleTheme()} className='flex gap-3 text-red-500'>
                            {
                                darkLight === "light" ? <MoonSvg/> : <SunSVg/>
                            }
                        </button>
                        <button className='text-smallText border border-transparent hover:border-cyan-600 hover:text-white px-4 py-2 rounded-md duration-200'>Login</button>
                        <button className='btn btn-primary'>Sign Up</button>
                    </div> */}
                </div>
            </div>

        </nav>
    )
}

export default Nav