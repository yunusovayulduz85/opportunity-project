import React from 'react'
import BlackPink from '../assets/blackPink.png'
import ExpertsAgree from '../assets/expertsAgree.png'
const StepThreeSection = () => {
    return (
        <>
            <section id='pricing' className='flex justify-center px-10 lg:pl-20 py-10'>
                <div className='container'>
                    <div>
                        <p className='text-blue-500 text-center font-bold'>STEP 3</p>
                        <h1 className='text-white text-center text-4xl font-bold tracking-wider py-2'>Send Emails & Text Messages</h1>
                        <p className='text-smallText text-center'>No more going through a social platform. Reach and engage <br />
                            your audience directly over email and text massage.</p>
                    </div>
                    <div className='flex items-center justify-between pt-20 responsiveContainer'>
                        <div>
                            <h4 className='text-white mb-10 mt-10'>Create & Share</h4>
                            <h2 className='text-white text-3xl sm:text-5xl font-bold tracking-wider mb-5'>Reach Your <br />
                                Audience<span className='text-blue-500'>Directly.</span></h2>
                            <div className='flex items-center gap-2'>
                                <div className='w-6 h-6 rounded-full bg-orderNumBg flex justify-center items-center'><p className='text-white'>1</p></div>
                                <p className='text-white text-lg'>Embed content or create something new to share.</p>
                            </div>
                            <div className='flex items-center gap-2 mt-2'>
                                <div className='w-6 h-6 rounded-full bg-orderNumBg flex justify-center items-center'><p className='text-white'>2</p></div>
                                <p className='text-white text-lg'>Share content over email, text message or your homepage.</p>
                            </div>
                            <div className='flex gap-4 mt-6 sm:flex-nowrap flex-wrap btnCenter'>
                                <button className='btn btn-primary'>Get Started Now</button>
                                <button className='text-smallText border  border-smallText px-6 py-2 rounded-md hover:border-blue-600 hover:text-white duration-200'>View A Demo</button>
                            </div>
                        </div>
                        <img className="w-responsiveImg xl:w-content" src={BlackPink} alt='black pink' />
                    </div>
                </div>
            </section>
            <div className='px-5 mt-10 flex justify-center' >
                <div className='container'>
                    <h2 className='text-white text-center text-3xl lg:text-5xl font-semibold mb-9'>Experts Agree</h2>
                    <img src={ExpertsAgree} alt='Experts Agree' />
                </div>
            </div>
            <div className='mt-24 trustControl flex justify-center items-center' >
                <h1 className='text-white font-semibold text-3xl lg:text-5xl text-center'>We've helped over 1,000 creatorsre <br />
                    claim control of their audience.</h1>
            </div>
        </>
    )
}

export default StepThreeSection