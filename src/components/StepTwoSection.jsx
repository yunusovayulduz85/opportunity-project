import React from 'react'
import Mobile from '../assets/Mobile.png'
const StepTwoSection = () => {
    return (
        <section id='about' className='flex justify-center px-20 py-16'>
            <div className='container'>
                <div>
                    <p className='text-blue-500 text-center font-bold'>STEP 2</p>
                    <h1 className='text-white text-center text-4xl font-bold tracking-wider py-2'>Share Your Homepage</h1>
                    <p className='text-smallText text-center'>Share your Wavium homepage link with your followers, and <br />
                        we'll handle the rest.</p>
                </div>
                <div className='flex gap-10 mt-20 flex-wrap md:flex-nowrap'>
                    <div className='bg-advertisingBg relative pt-7 pl-7 flex-1'>
                        <p className='text-headerPoint mb-5 text-xs md:text-base'>One Link</p>
                        <h2 className='text-white text-xl sm:text-3xl lg:text-6xl font-bold mb-5'><span className='text-advertisingText'>ALL You Create.</span> <br />One Link</h2>
                        <div >
                            <img className='lg:ml-phoneImg' width={550} src={Mobile} alt='Everything you create is in one link' />
                        </div>
                    </div>
                    <div className='bg-advertisingBg relative pt-7 pl-7 flex-1'>
                        <p className='text-headerPoint mb-12 lg:mb-16  text-xs md:text-base'>Collect Subscribers</p>
                        <h2 className='text-white text-xl sm:text-3xl lg:text-6xl font-bold mb-5'><span className='text-advertisingText'>Emails. <br />
                            Phone #s.</span> <br />All Yours.</h2>
                    </div>
                </div>
                <div className='flex justify-center mt-6'>
                    <button className='btn btn-primary px-11 py-3'>Get Started </button>
                </div>
            </div>
        </section>
    )
}

export default StepTwoSection