import React from 'react'
import SincereIconSvg from '../icons/SincereIconSvg'
import HappyIconSvg from '../icons/HappyIconSvg'
import PartIconSvg from '../icons/PartIconSvg'

const AdvertisingSection = () => {
    return (
        <section className='flex justify-center px-10 md:20 lg:px-28'>
            <div className='container'>
                <h2 className='text-white text-center text-2xl md:text-4xl font-semibold tracking-wider'>Why Creators Love Marico</h2>
                <div className='flex justify-center gap-10 md:gap-0 md:justify-between pt-10 items-center flex-wrap md:flex-nowrap'>
                    <div>
                        <h4 className='text-white text-lg md:text-xl font-bold justify-center flex gap-3 items-center'>
                            <SincereIconSvg />
                            Reduced Anxiety</h4>
                        <p className='text-smallText text-center text-sm pt-1'>Never worry about losing your audience.</p>
                    </div>
                    <div>
                        <h4 className='text-white text-lg md:text-xl font-bold justify-center flex gap-3 items-center'>
                            <HappyIconSvg />
                            Lower Workload</h4>
                        <p className='text-smallText text-center text-sm pt-1'>Just share one link. We'll handle the rest.</p>
                    </div>
                    <div>
                        <h4 className='text-white justify-center flex gap-3 items-center text-lg md:text-xl font-bold'>
                            <PartIconSvg />
                            More Time</h4>
                        <p className='text-smallText text-center text-sm pt-1'>Never worry about losing your audience.</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AdvertisingSection