import React from 'react'

const Header = () => {
    return (
        <header id='header' className='pt-32 pb-20 px-12 flex justify-center'>
            <div className='container'>
                <h1 className='text-white text-center text-4xl md:text-6xl font-extrabold animation-title'>Own your audience. <br /><span className='header-text font-extrabold'>So you don't lose them</span></h1>
                <p>Turn your audience into email and
                    text message subscribers.</p>
                <div className='flex gap-4 justify-center sm:flex-nowrap flex-wrap'>
                    <button className='btn btn-primary'>Get Started Now</button>
                    <button className='text-smallText border  border-smallText px-6 py-2 rounded-md hover:border-blue-600 hover:text-white duration-200'>View A Demo</button>
                </div>
                <div className='flex items-center gap-2 justify-center pt-4'>
                    <div className='w-6 h-6 rounded-full header-point flex justify-center items-center'>
                        <div className='w-3 h-3 rounded-full bg-headerPoint'></div>
                    </div>
                    <p className='text-smallText'>
                        <span className='font-bold text-white text-sm md:text-base'>1000+</span>
                        creators have already started
                    </p>
                </div>
            </div>
        </header>
    )
}

export default Header