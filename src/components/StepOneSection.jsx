import React from 'react'
import Content from "../assets/content.png"

const StepOneSection = () => {
    return (
        <section id='usecases' className='flex justify-center px-10 lg:pl-20 py-16'>
            <div className='container'>
                <div>
                    <p className='text-blue-500 text-center font-bold'>STEP 1</p>
                    <h1 className='text-white text-center text-4xl font-bold tracking-wider py-2'>Connect Your Content</h1>
                    <p className='text-smallText text-center'>Bring all of your content together and get a Homepage that <br />
                        automatically updates whenever you create anywhere online.</p>
                    <div className='flex justify-center pt-4'>
                        <button className='text-smallText px-5 py-2 rounded-md hover:border-blue-500 hover:text-white duration-200 border border-smallText'>View Avaliable Sources</button>
                    </div>
                </div>
                <div className='flex items-center justify-between pt-20 responsiveContainer'>
                    <div >
                        <h4 className='text-white mb-10 mt-10'>Your Homepage</h4>
                        <h2 className='text-white text-4xl md:text-5xl font-bold tracking-wider mb-5'>Your Content. <br />All in <span className='text-blue-500'>One Spot</span></h2>
                        <div className='flex items-center gap-2'>
                            <div className='w-6 h-6 rounded-full bg-orderNumBg flex justify-center items-center'><p className='text-white'>1</p></div>
                            <p className='text-white text-lg'>Bring all of your content together into one homepage.</p>
                        </div>
                        <div className='flex items-center gap-2 mt-2 '>
                            <div className='w-6 h-6 rounded-full bg-orderNumBg flex justify-center items-center'><p className='text-white'>2</p></div>
                            <p className='text-white text-xl'>Your page automatically updates whenever you create.</p>

                        </div>
                        <div className='flex gap-4 mt-6 sm:flex-nowrap flex-wrap btnCenter'>
                            <button className='btn btn-primary'>Get Started Now</button>
                            <button className='text-smallText border  border-smallText px-6 py-2 rounded-md hover:border-blue-600 hover:text-white duration-200'>View A Demo</button>
                        </div>
                    </div>
                    <img  className="right-div w-responsiveImg xl:w-content" src={Content} alt='new content' />
                </div>
            </div>
        </section>
    )
}

export default StepOneSection