import './App.css';
import AdvertisingSection from './components/AdvertisingSection';
import FooterSection from './components/FooterSection';
import Header from './components/Header';
import Nav from './components/Nav';
import StepOneSection from './components/StepOneSection';
import StepThreeSection from './components/StepThreeSection';
import StepTwoSection from './components/StepTwoSection';

function App() {
  return (
    <>
      <Nav />
      <Header />
      <AdvertisingSection />
      <StepOneSection/>
      <StepTwoSection/>
      <StepThreeSection/>
      <FooterSection/>
    </>
  );
}

export default App;
