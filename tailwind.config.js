/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      height: {
        navHeight: "70px"
      },
      colors: {
        smallText: "#707070",
        headerPoint: "#0FC65C",
        orderNumBg: "#2D2D2D",
        advertisingText: "#919191",
        advertisingBg: "#0D0D0D"
      },
      margin: {
        phoneImg:"164px",
        phoneImg2:"70px",
      },
      width:{
        content:"700px",
        responsiveImg:"500px"
      }
    },
  },
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'),
  ],
}

